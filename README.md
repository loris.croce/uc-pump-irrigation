# Use case pump irrigation

![](uc_pump.png)

This application aims to water a potted plant with a pump following two rules:
  
- Activate the pump if the water tank is not empty.
- Activate the pump if the soil is below a *dryness* threshold. 

The message sent in CBOR with LoRa every minute is of the following form: 

```json
{
  "nid": "ABCD",
  "lvl": 85,
  "pmp": false,
  "rfl": false, 
  "mst": 53
}
```

With `nid` the node ID, 4 last digits of the MAC address, `lvl` the water tank level, `pmp` was the pump activated, `rfl` does the tank needs to be filled and `mst` the soil moisture %.

## Notes

- `Serial1` : Green D5 Blue D6 for A02YYUW sensor.
- This setup isn't battery proof (yet?) because deep sleep appears to still power the pump and the battery is not powerful 
enough for the pump.

## Moisture sensor calibration

```cpp
int rmax = 0;
int rmin = 3000;

void calibration(int raw)
{
  if (raw > rmax)
    rmax = raw;
  if (raw < rmin)
    rmin = raw;
  Serial.printf("raw: %d, rmax: %d, rmin: %d\n", raw, rmax, rmin);
}
```