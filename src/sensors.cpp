#include "sensors.h"

DistanceSensor_A02YYUW distanceSensor(&Serial1);
// Adafruit_LC709203F lc;

bool dry = false;
bool watering = false;
bool empty = false;

int getLevel()
{
    DistanceSensor_A02YYUW_MEASSUREMENT_STATUS meassurementStatus;
    int distance;
    do
    {
        meassurementStatus = distanceSensor.meassure();
        if (meassurementStatus == DistanceSensor_A02YYUW_MEASSUREMENT_STATUS_OK)
        {
            distance = distanceSensor.getDistance();
        }
    } while (meassurementStatus != DistanceSensor_A02YYUW_MEASSUREMENT_STATUS_OK);
    int level = map(distance, MIN_LEVEL, MAX_LEVEL, 0, 100);
    level = constrain(level, 0, 100);
    empty = (level < LEVEL_THRESHOLD);
    return level;
    // SEND LEVEL MESSAGE
}

int getMoisture()
{
    int raw = analogRead(MOISTURE_PIN);
    int moisture = map(raw, FULL_DRY, FULL_WET, 0, 100);
    dry = (moisture < MOIST_THRESHOLD);
    return constrain(moisture, 0, 100);
}

void activatePump()
{
    watering = (getLevel() > LEVEL_THRESHOLD && getMoisture() < MOIST_THRESHOLD);
    if (watering)
    {
        watering = true;
        digitalWrite(PUMP_PIN, HIGH);
        delay(TIME_PUMP);
        digitalWrite(PUMP_PIN, LOW);
    }
}
