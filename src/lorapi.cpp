#include "lorapi.h"

uint8_t msg_buf[BUF_SIZE];
uint8_t mac_address[6];

RTC_DATA_ATTR uint64_t counter = 1;

char *getId()
{
    uint8_t baseMac[6];
    // Get MAC address for WiFi station
    esp_read_mac(baseMac, ESP_MAC_WIFI_STA);
    static char baseMacChr[5];
    sprintf(baseMacChr, "%02X%02X", baseMac[4], baseMac[5]);
    return baseMacChr;
}

void initLoRa()
{
    Serial.println("LoRa Sender");

    LoRa.setPins(NSS, NRESET);
    LoRa.setCodingRate4(CODING_RATE_DENOMINATOR);
    LoRa.setSignalBandwidth(SIGNAL_BANDWIDTH);
    LoRa.setSyncWord(SYNC_WORD);
    LoRa.setSpreadingFactor(SPREADING_FACTOR);

    LoRa.setTxPower(TX_POWER);

    if (!LoRa.begin(FREQUENCY))
    {
        while (1)
        {
            Serial.println("Starting LoRa failed!");
            delay(2000);
        }
    }
}

void sendMsg()
{
    // if and boolean for watering, refill etc.
    TinyCBOR.init();
    TinyCBOR.Encoder.init(msg_buf, BUF_SIZE);

    TinyCBOR.Encoder.create_map(6);
    TinyCBOR.Encoder.encode_text_stringz("nid");
    TinyCBOR.Encoder.encode_text_string(getId(), 4);
    TinyCBOR.Encoder.encode_text_stringz("lvl");
    TinyCBOR.Encoder.encode_int(getLevel());
    TinyCBOR.Encoder.encode_text_stringz("pmp");
    TinyCBOR.Encoder.encode_boolean(watering);
    TinyCBOR.Encoder.encode_text_stringz("rfl");
    TinyCBOR.Encoder.encode_boolean(empty);
    TinyCBOR.Encoder.encode_text_stringz("mst");
    TinyCBOR.Encoder.encode_int(getMoisture());
    TinyCBOR.Encoder.encode_text_stringz("cnt");
    TinyCBOR.Encoder.encode_uint(counter);
    // TinyCBOR.Encoder.encode_text_stringz("bat");
    // TinyCBOR.Encoder.encode_float(lc.cellVoltage());
    TinyCBOR.Encoder.close_container();

    LoRa.beginPacket();
    LoRa.write(msg_buf, BUF_SIZE);
    LoRa.endPacket();

    TinyCBOR.Parser.init(msg_buf, BUF_SIZE, 0);
    TinyCBOR.Parser.to_json(Serial);
    Serial.println();

    counter++;
}