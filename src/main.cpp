#include <Arduino.h>

#include "config.h"
#include "sensors.h"
#include "lorapi.h"

void setup()
{
  Serial.begin(115200);
  Serial1.begin(9600);

  pinMode(PUMP_PIN, OUTPUT);
  digitalWrite(PUMP_PIN, LOW);
  pinMode(MOISTURE_PIN, INPUT);
  // lc.begin();

  initLoRa();
}

void loop()
{
  activatePump();
  sendMsg();

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  esp_light_sleep_start();
}
