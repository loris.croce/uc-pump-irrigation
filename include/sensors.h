#ifndef SENSORS_H
#define SENSORS_H

#include <DistanceSensor_A02YYUW.h>
#include "config.h"

extern DistanceSensor_A02YYUW distanceSensor;
// extern Adafruit_LC709203F lc;

int getLevel();
int getMoisture();
void activatePump();

extern bool dry;
extern bool watering;
extern bool empty;

#endif