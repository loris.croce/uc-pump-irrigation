#ifndef LORAPI_H
#define LORAPI_H

#include <LoRa.h>
#include <SPI.h>
#include <tinycbor.h>
#include "config.h"
#include "sensors.h"

char *getId();
void initLoRa();
void sendMsg(); // if and boolean for watering, refill etc.

#endif