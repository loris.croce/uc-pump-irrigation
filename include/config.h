#ifndef CONFIG_H
#define CONFIG_H

// LoRa

#define FREQUENCY 868E6
#define SIGNAL_BANDWIDTH 125E3
#define CODING_RATE_DENOMINATOR 8
#define SPREADING_FACTOR 7
#define PREAMBLE_LENGTH 8
#define SYNC_WORD 0x12
#define TX_POWER 20
#define LNA_GAIN 6

#define BUF_SIZE 48

#define NSS D4
#define NDIO0 D5
#define NRESET D2

// Watering

#define FULL_DRY 2230
#define FULL_WET 1555
#define PUMP_PIN D9
#define MOISTURE_PIN A4

#define MAX_LEVEL 30
#define MIN_LEVEL 170

#define MOIST_THRESHOLD 25
#define LEVEL_THRESHOLD 10
#define TIME_PUMP 5000

// Deep Sleep

#define DELAY 5000
#define uS_TO_S_FACTOR 1000000 /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 60       /* Time ESP32 will go to sleep (in seconds) */

#endif